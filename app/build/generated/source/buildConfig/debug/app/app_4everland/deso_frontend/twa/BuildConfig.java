/**
 * Automatically generated file. DO NOT MODIFY
 */
package app.app_4everland.deso_frontend.twa;

public final class BuildConfig {
  public static final boolean DEBUG = Boolean.parseBoolean("true");
  public static final String APPLICATION_ID = "app.app_4everland.deso_frontend.twa";
  public static final String BUILD_TYPE = "debug";
  public static final int VERSION_CODE = 1;
  public static final String VERSION_NAME = "1.0.0.0";
}
